export function getWinner(match){
    if(match.result?.fullTime?.home > match.result?.fullTime?.visitors){
        return "home"
    }
    if(match.result?.fullTime?.home < match.result?.fullTime?.visitors){
        return "visitors"
    }
    return "none"
}

export function hasBeenPlayed(match){
    return !!match.result
}