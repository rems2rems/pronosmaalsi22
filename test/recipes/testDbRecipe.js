import { expect } from "chai";
import mongoose from "mongoose";
import { DbRecipe } from "../../src/models/DbRecipe.js";

describe('test Db Recipe', () => {
    let connection
    beforeEach(async () => {
        connection = await mongoose.connect("mongodb://localhost:27017/cordonbleu",{connectTimeoutMS:1000})
    })
    afterEach(async () => {
        connection.disconnect()
    })
    it('Should create a recipe in db', async () => {
        const dbRecipe = new DbRecipe({
            name : "Mi-goreng",
            needOven : true,
            ingredients : ["rice","egg","spices"],
            isExotic : true,
            originCountry : "Indonesia",
            needSpecificTools : true
        })
        await dbRecipe.save()
        const recipes = await DbRecipe.find()
        expect(recipes.length).to.be.greaterThanOrEqual(1)
    });
    // afterEach(async () => {
    //     return DbRecipe.deleteMany({})
    // })
});