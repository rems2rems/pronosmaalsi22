import { expect } from "chai";
import { JSDOM } from "jsdom";
import mongoose from "mongoose";
import request from "supertest";
import { createApp } from "../../src/createApp.js";

describe('Pages tests', async () => {
    let connection
    beforeEach(async () => {
        connection = await mongoose.connect("mongodb://localhost:27017/cordonbleu",{connectTimeoutMS:1000})
    })
    afterEach(async () => {
        connection.disconnect()
    })
    it('homepage displays recipes', (done) => {
        const app = createApp().then((app) => {
            request(app)
                .get('/home')
                .end(function (err, res) {
                    if (err) {
                        console.log(err);
                        throw err;
                    }
                    const {window:{document}} = new JSDOM(res.text);
                    const title = document.querySelector("h1")
                    expect(title).to.be.ok
                    expect(title.textContent).to.eql("Bienvenue sur Cordon-bleu!")
                    done()
                });
        })
    });
});