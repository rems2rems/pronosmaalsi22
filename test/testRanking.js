import { expect } from "chai"
import { createTournament } from "./fixtures.js"
import { createRanking } from "../src/business/ranking.js"

describe('Ranking tests', async () => {
    it('1 match, 2 betters, no malus', () => {

        const tournament = createTournament()
        const ranking = createRanking(tournament)
        expect(ranking).to.be.ok
        expect(ranking.length).to.equal(2)
        expect(ranking[0].name).to.equal("Ana")
        expect(ranking[0].points).to.equal(2)
        expect(ranking[1].name).to.equal("Bob")
        expect(ranking[1].points).to.equal(0)
    })
    it('1 match, 2 betters, 1 malus', () => {

        const tournament = createTournament()
        tournament.bets[1].result.fullTime.home = 0
        tournament.bets[1].result.fullTime.visitors = 1
        const ranking = createRanking(tournament)
        expect(ranking).to.be.ok
        expect(ranking.length).to.equal(2)
        expect(ranking[0].name).to.equal("Ana")
        expect(ranking[0].points).to.equal(2)
        expect(ranking[1].name).to.equal("Bob")
        expect(ranking[1].points).to.equal(-1)
    })
})