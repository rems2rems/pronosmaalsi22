import { expect } from "chai"
import { createTournament } from "./fixtures.js"
import { getWinner, hasBeenPlayed } from "../src/business/winner.js"

describe('match played tests', async () => {
    it('match has not been played', () => {

        const match = {
            _id: 1,
            home: 1,
            visitors: 2
        }
        const played = hasBeenPlayed(match)
        expect(played).to.equal(false)
    })
    it('match has been played', () => {

        const match = {
            _id: 1,
            home: 1,
            visitors: 2,
            result: {
                fullTime: {
                    home: 0,
                    visitors: 0
                }
            }
        }
        const played = hasBeenPlayed(match)
        expect(played).to.equal(true)
    })
})
describe('Winner tests', async () => {
    it('home wins', () => {

        const match = {
            _id: 1,
            home: 1,
            visitors: 2,
            result: {
                fullTime: {
                    home: 3,
                    visitors: 2
                }
            }
        }
        const winner = getWinner(match)
        expect(winner).to.equal("home")
    })
    it('visitors win', () => {

        const match = {
            _id: 1,
            home: 1,
            visitors: 2,
            result: {
                fullTime: {
                    home: 1,
                    visitors: 2
                }
            }
        }
        const winner = getWinner(match)
        expect(winner).to.equal("visitors")
    })
    it('draw match', () => {

        const match = {
            _id: 1,
            home: 1,
            visitors: 2,
            result: {
                fullTime: {
                    home: 0,
                    visitors: 0
                }
            }
        }
        const winner = getWinner(match)
        expect(winner).to.equal("none")
    })
})